lines = []
with open('day10.txt', 'r') as data:
    lines = data.readlines()

# task 1
num_cycles = 0
cycles_to_check = [20, 60, 100, 140, 180, 220]
signal_strength = 0
number_of_cycles = {'noop': 1, 'addx': 2}
x_val = 1
for line in lines:
    command = line.strip().split(' ')
    instruction = command[0]
    val = 0
    if len(command) == 2:
        val = int(command[1])
    
    for i in range(number_of_cycles[instruction]):
        num_cycles += 1
        if num_cycles in cycles_to_check:
            signal_strength += x_val * num_cycles

    x_val += val

print(signal_strength)


#task 2
num_cycles = 0
screen = [['#' for _ in range(40)] for _ in range(6)]
sprite_line = 0
sprite_pos = 1
x_val = 1

for line in lines:
    command = line.strip().split(' ')
    instruction = command[0]
    val = 0
    if len(command) == 2:
        val = int(command[1])
    
    for i in range(number_of_cycles[instruction]):
        if not (num_cycles in [sprite_pos-1, sprite_pos, sprite_pos+1]):
            screen[sprite_line][num_cycles] = '.'

        num_cycles += 1

        if num_cycles == 40:
            sprite_line +=1
            num_cycles = 0
            

    x_val += val
    sprite_pos = x_val
        
for line in screen:
    print(''.join(line))