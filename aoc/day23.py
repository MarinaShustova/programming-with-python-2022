from copy import deepcopy

lines = []
with open('day23.txt', 'r') as data:
    lines = data.readlines()

first_mat = []
elves_count = 0
for line in lines:
    temp_list = []
    for i in line.strip():
        temp_list.append(i.strip())
    elves_count += temp_list.count('#')
    first_mat.append(temp_list)


def get_elvish_coords(matrix):
    elvish_coor = []
    m = len(matrix[0])
    n = len(matrix)
    for i in range(n):
        for j in range(m):
            if matrix[i][j] == "#":
                elvish_coor.append((i, j))
    return elvish_coor


def will_not_move(matrix, curr_coord):
    curr_r = curr_coord[0]
    curr_c = curr_coord[1]

    n = matrix[curr_r-1][curr_c]
    s = matrix[curr_r+1][curr_c]
    w = matrix[curr_r][curr_c-1]
    e = matrix[curr_r][curr_c+1]
    nw = matrix[curr_r-1][curr_c-1]
    sw = matrix[curr_r+1][curr_c-1]
    ne = matrix[curr_r-1][curr_c+1]
    se = matrix[curr_r+1][curr_c+1]

    if w == "." and s == "." and e == "." and n == "." and ne == "." and nw == "." and se == "." and sw == ".":
        return True

    return False


def can_move_north(matrix, coord):
    if matrix[coord[0] - 1][coord[1] - 1] == "#" or matrix[coord[0] - 1][coord[1]] == "#" or matrix[coord[0] - 1][coord[1] + 1] == "#":
        return False
    return True


def can_move_south(matrix, coord):
    if matrix[coord[0] + 1][coord[1] - 1] == "#" or matrix[coord[0] + 1][coord[1]] == "#" or matrix[coord[0] + 1][coord[1] + 1] == "#":
        return False
    return True


def can_move_west(matrix, coord):
    if matrix[coord[0] - 1][coord[1] - 1] == "#" or matrix[coord[0]][coord[1] - 1] == "#" or matrix[coord[0] + 1][coord[1] - 1] == "#":
        return False
    return True


def can_move_east(matrix, coord):
    if matrix[coord[0] - 1][coord[1] + 1] == "#" or matrix[coord[0]][coord[1] + 1] == "#" or matrix[coord[0] + 1][coord[1] + 1] == "#":
        return False
    return True


def perform_step(matrix, iteration, ec):

    first_matrix = deepcopy(matrix)

    new_n = len(matrix) + 2
    new_m = len(matrix[0]) + 2
    matrix.insert(0, ["." for _ in range(new_m)])
    matrix.append(["." for _ in range(new_m)])
    for i in range (1, new_n - 1):
        matrix[i].insert(0, '.')
        matrix[i].append('.')

    elf_coordinates = get_elvish_coords(matrix)

    coordinates_to_move = []
    for coord in elf_coordinates:
        if will_not_move(matrix, coord):
            coordinates_to_move.append(coord)
            continue
        elif iteration == 1:
            if can_move_north(matrix, coord):
                coordinates_to_move.append((coord[0]-1, coord[1]))
            elif can_move_south(matrix, coord):
                coordinates_to_move.append((coord[0]+1, coord[1]))
            elif can_move_west(matrix, coord):
                coordinates_to_move.append((coord[0], coord[1]-1))
            elif can_move_east(matrix, coord):
                coordinates_to_move.append((coord[0], coord[1]+1))
            else:
                coordinates_to_move.append((coord[0], coord[1]))
        elif iteration == 2:
            if can_move_south(matrix, coord):
                coordinates_to_move.append((coord[0] + 1, coord[1]))
            elif can_move_west(matrix, coord):
                coordinates_to_move.append((coord[0], coord[1] - 1))
            elif can_move_east(matrix, coord):
                coordinates_to_move.append((coord[0], coord[1] + 1))
            elif can_move_north(matrix, coord):
                coordinates_to_move.append((coord[0] - 1, coord[1]))
            else:
                coordinates_to_move.append((coord[0], coord[1]))
        elif iteration == 3:
            if can_move_west(matrix, coord):
                coordinates_to_move.append((coord[0], coord[1] - 1))
            elif can_move_east(matrix, coord):
                coordinates_to_move.append((coord[0], coord[1] + 1))
            elif can_move_north(matrix, coord):
                coordinates_to_move.append((coord[0] - 1, coord[1]))
            elif can_move_south(matrix, coord):
                coordinates_to_move.append((coord[0] + 1, coord[1]))
            else:
                coordinates_to_move.append((coord[0], coord[1]))
        elif iteration == 4:
            if can_move_east(matrix, coord):
                coordinates_to_move.append((coord[0], coord[1] + 1))
            elif can_move_north(matrix, coord):
                coordinates_to_move.append((coord[0] - 1, coord[1]))
            elif can_move_south(matrix, coord):
                coordinates_to_move.append((coord[0] + 1, coord[1]))
            elif can_move_west(matrix, coord):
                coordinates_to_move.append((coord[0], coord[1] - 1))
            else:
                coordinates_to_move.append((coord[0], coord[1]))

    if coordinates_to_move == elf_coordinates:
        return first_matrix

    for i in range(ec):
        if coordinates_to_move.count(coordinates_to_move[i]) == 1 and coordinates_to_move[i] not in elf_coordinates:
            matrix[elf_coordinates[i][0]][elf_coordinates[i][1]] = '.'
            matrix[coordinates_to_move[i][0]][coordinates_to_move[i][1]] = '#'
    
    return matrix


def rectangle_calc(matrix):
    min_row = 0
    min_col = 0
    max_row = len(matrix)
    max_col = len(matrix[0])

    break_flag = False
    for r in matrix:
        for c in r:
            if "#" in c:
                break_flag = True
                break
        if break_flag:
            break
        min_row += 1

    break_flag = False
    for r in range(max_row, min_row, -1):
        for c in matrix[r-1]:
            if "#" in c:
                break_flag = True
                break
        if break_flag:
            break
        max_row -= 1

    break_flag = False
    for c in range(len(matrix[0])):
        for r in range(len(matrix)):
            if matrix[r][c] == "#":
                break_flag = True
                break
        if break_flag:
            break
        min_col += 1

    break_flag = False
    for c in range(len(matrix[0])-1, 0, -1):
        for r in range(len(matrix)):
            if matrix[r][c] == "#":
                break_flag = True
                break
        if break_flag:
            break
        max_col -= 1

    return min_row, min_col, max_row, max_col


prev_list = deepcopy(first_mat)
iteration = 1

for i in range(10):
    first_mat = perform_step(first_mat, iteration, elves_count)
    iteration += 1
    if iteration == 5:
        iteration = 1


min_row, min_col, max_row, max_col = rectangle_calc(first_mat)

elves_covered_area = (max_row-min_row) * (max_col-min_col)

empty_grounds = elves_covered_area - elves_count
print(empty_grounds)

# task 2

first_mat = deepcopy(prev_list)

i = 1
iteration = 1
while True:
    first_mat = perform_step(first_mat, iteration, elves_count)
    iteration += 1
    if iteration == 5:
        iteration = 1
    if prev_list == first_mat:
        print(i)
        break
    else:
        prev_list = deepcopy(first_mat)
    i += 1
