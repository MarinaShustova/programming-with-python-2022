input = ''
with open('day06.txt', 'r') as data:
    input = data.readline().strip()

last_four = list(input[:4])
n = len(input)

result = -1

for i in range(4, n):
    if len(set(last_four)) == 4:
        result = i
        break
    del last_four[0]
    last_four.append(input[i])

print(result)


last_fourteen = list(input[:14])

result = -1

for i in range(14, n):
    if len(set(last_fourteen)) == 14:
        result = i
        break
    del last_fourteen[0]
    last_fourteen.append(input[i])

print(result)