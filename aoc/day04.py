lines = []
with open('day04.txt', 'r') as data:
    lines = data.readlines()

# task 1
overlap_count = 0
for line in lines:
    ranges = line.strip().split(',')
    bord_1 = ranges[0].split('-')
    bord_2 = ranges[1].split('-')
    if (int(bord_1[0]) <= int(bord_2[0]) and int(bord_2[1]) <= int(bord_1[1])) or (int(bord_2[0]) <= int(bord_1[0]) and int(bord_1[1]) <= int(bord_2[1])):
        overlap_count += 1
print(overlap_count)

# task 2
overlap_count = 0
for line in lines:
    ranges = line.strip().split(',')
    bord_1 = ranges[0].split('-')
    bord_2 = ranges[1].split('-')
    if max(int(bord_1[0]), int(bord_2[0])) <= min(int(bord_1[1]), int(bord_2[1])):
        overlap_count += 1
print(overlap_count)