lines = []
with open('day07.txt', 'r') as data:
    lines = data.readlines()

# task 1
weights = {}
tree = {}
directories = []
dir = ''
for line in lines:
    if line.startswith('$ cd'):
        if line.startswith('$ cd ..'):
            for k in tree:
                if dir in tree[k]:
                    dir = k
                    break
        else:
            slash = '' if dir == '' else '/'
            dir = dir + line.strip().split(' ')[2] + slash
            directories.append(dir)
            tree[dir] = set()
            weights[dir] = 0
    elif line.startswith('dir'):
        tree[dir].add(dir + line.strip().split(' ')[1] + '/')
    elif line.startswith('$ ls'):
        continue
    else:
        file = line.strip().split(' ')
        file_name = dir + file[1] + '/'
        tree[dir].add(file_name)
        tree[file_name] = set()
        weights[file_name] = int(file[0])

def sum_weights(graph, node, visited=None):
    if visited is None:
        visited = set()
    visited.add(node)
    for next in graph[node] - visited:
        weights[node] += sum_weights(graph, next, visited)
    return weights[node]

sum_weights(tree, '/')

total_sum = 0
for file in weights:
    if file in directories:
        if weights[file] <= 100000:
            total_sum += weights[file]

print(total_sum)

# task 2
total_space = 70000000
free_space_needed = 30000000
free_space_available = total_space - weights['/']
min_size_to_delete = free_space_needed - free_space_available

#it is safe to assign this value and treat it as an infinitely large
file_size_to_delete = 70000000

for file in weights:
    if file in directories:
        if weights[file] > min_size_to_delete and weights[file] < file_size_to_delete:
            file_size_to_delete = weights[file]

print(file_size_to_delete)
