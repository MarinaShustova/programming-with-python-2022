lines = []
with open('day12.txt', 'r') as data:
    lines = data.readlines()

G = {}

n = len(lines)
m = len(lines[0]) - 1

a_positions = []

start_node = 0
end_node = (n - 1) * (m - 1)

for i in range(n):
    for j in range(m):
        G[i * m + j] = []
        curr_pos = lines[i][j]
        if curr_pos == 'a':
            a_positions.append(i * m + j)
        if curr_pos == 'S':
            start_node = i * m + j
            lines[i] = lines[i].replace('S', 'a')
            a_positions.append(i * m + j)
        elif curr_pos == 'E':
            end_node = i * m + j
            lines[i] = lines[i].replace('E', 'z')

for i in range(n):
    for j in range(m):
        curr_pos = lines[i][j]    
        if i < n - 1:
            if (ord(curr_pos) + 1) >= ord(lines[i + 1][j]):
                G[i * m + j].append((i+1) * m + j)
        if i > 0:
            if (ord(curr_pos) + 1) >= ord(lines[i - 1][j]):
                G[i * m + j].append((i-1) * m + j)
        if j < m - 1:
            if (ord(curr_pos) + 1) >= ord(lines[i][j + 1]):
                G[i * m + j].append(i * m + j + 1)
        if j > 0:
            if (ord(curr_pos) + 1) >= ord(lines[i][j - 1]):
                G[i * m + j].append(i * m + j - 1)

# Just apply Dijkstra's algorithm

def find_shortest_path(start_node):
    unvisited_nodes = list(range(n * m))
 
    shortest_path = {}
    previous_nodes = {}

    for node in unvisited_nodes:
        shortest_path[node] = float('inf')

    shortest_path[start_node] = 0
    
    while unvisited_nodes:
        curr_min_node = None
        for node in unvisited_nodes: 
            if curr_min_node == None:
                curr_min_node = node
            elif shortest_path[node] < shortest_path[curr_min_node]:
                curr_min_node = node
            
        neighbors = G[curr_min_node]
        for neighbor in neighbors:
            tentative_value = shortest_path[curr_min_node] + 1
            if tentative_value < shortest_path[neighbor]:
                shortest_path[neighbor] = tentative_value
                previous_nodes[neighbor] = curr_min_node

        unvisited_nodes.remove(curr_min_node)

    return previous_nodes, shortest_path


previous_nodes, shortest_path = find_shortest_path(start_node)

    
print("{}".format(str(shortest_path[end_node])))

# task 2

min_path = float('inf')

print(len(a_positions))

for pos in a_positions:
    print(a_positions.index(pos))
    previous_nodes, shortest_path = find_shortest_path(pos)
    if shortest_path[end_node] < min_path:
        min_path = shortest_path[end_node]

print(min_path)
