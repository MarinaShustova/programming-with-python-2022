lines = []
with open('day20.txt', 'r') as data:
    lines = data.readlines()

class Node:
    def __init__(self, val):
        self.value = val
        self.prev = None
        self.next = None

class DoubleLinkedList:
    def __init__(self):
        self.tail = None
    
    def add_element(self, node):
        if self.tail == None:
            self.tail = node
            node.next = node
            node.prev = node
        else:
            node.prev = self.tail
            node.next = self.tail.next
            self.tail.next.prev = node
            self.tail.next = node
            self.tail = node

    def rotate_elem(self, offset, node):
        count = abs(offset)
        if offset == 0:
            return
        if offset > 0:
            target_node = node.next
            while count > 0:
                target_node = target_node.next
                if target_node == node:
                    continue
                count -= 1
            if target_node == node:
                return
            node.prev.next = node.next
            node.next.prev = node.prev
            node.next = target_node
            node.prev = target_node.prev
            target_node.prev.next = node
            target_node.prev = node
        else:
            target_node = node.prev
            while count > 0:
                target_node = target_node.prev
                if target_node == node:
                    continue
                count -= 1
            if target_node == node:
                return
            node.prev.next = node.next
            node.next.prev = node.prev
            node.prev = target_node
            node.next = target_node.next
            target_node.next.prev = node
            target_node.next = node
        

    def get_i_th_value(self, i, node):
        target_node = node.next
        while i > 1:
            target_node = target_node.next
            i -= 1
        return target_node.value


    def print_n_elements(self, node_x, n):
        node = node_x.next
        res = []
        while n > 0:
            res.append(node.value)
            node = node.next
            n -= 1
        print(' '.join(map(str, res)))

list_of_nodes = []
dll = DoubleLinkedList()
zero_node = None

for line in lines:
    elem = int(line.strip())
    node = Node(elem)
    if elem == 0:
        zero_node = node
    list_of_nodes.append(node)
    dll.add_element(node)


n = len(list_of_nodes)

for i in range(n):
    dll.rotate_elem(list_of_nodes[i].value, list_of_nodes[i])
    # dll.print_n_elements(zero_node, n)


val_list = [dll.get_i_th_value(1000, zero_node), dll.get_i_th_value(2000, zero_node), dll.get_i_th_value(3000, zero_node)]

print(val_list)
print(sum(val_list))
