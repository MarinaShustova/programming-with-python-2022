# task 1
outcomes = {'AY' : 8, 'AX' : 4, 'AZ' : 3, 'BY' : 5, 'BX' : 1, 'BZ' : 9, 'CY' : 2, 'CX': 7, 'CZ' : 6}
with open('day02.txt', 'r') as data:
    total = 0
    lines = data.readlines()
    for line in lines:
        key = ''.join(line.strip().split(' '))
        total += outcomes[key]
    print(total)

# task 2
# just re-calculate the outcomes according to the new rules
outcomes = {'AY' : 4, 'AX' : 3, 'AZ' : 8, 'BY' : 5, 'BX' : 1, 'BZ' : 9, 'CY' : 6, 'CX': 2, 'CZ' : 7}
with open('day02.txt', 'r') as data:
    total = 0
    lines = data.readlines()
    for line in lines:
        key = ''.join(line.strip().split(' '))
        total += outcomes[key]
    print(total)