lines = []
with open('day18.txt', 'r') as data:
    lines = data.readlines()

cubes = []

for line in lines:
    coords = line.strip().split(',')
    cubes.append((int(coords[0]), int(coords[1]), int(coords[2])))

surface = 0

max_x = 0
max_y = 0
max_z = 0

min_x = float('inf')
min_y = float('inf')
min_z = float('inf')


for cube in cubes:
    neighbours = 0

    if cube[0] > max_x:
        max_x = cube[0]
    if cube[1] > max_y:
        max_y = cube[1]
    if cube[2] > max_z:
        max_z = cube[2]

    if cube[0] < min_x:
        min_x = cube[0]
    if cube[1] < min_y:
        min_y = cube[1]
    if cube[2] < min_z:
        min_z = cube[2]


    if (cube[0], cube[1], cube[2] - 1) in cubes:
        neighbours += 1
    
    if (cube[0], cube[1], cube[2] + 1) in cubes:
        neighbours += 1

    if (cube[0], cube[1] - 1, cube[2]) in cubes:
        neighbours += 1
    
    if (cube[0], cube[1] + 1, cube[2]) in cubes:
        neighbours += 1
    
    if (cube[0] - 1, cube[1], cube[2]) in cubes:
        neighbours += 1
    
    if (cube[0] + 1, cube[1], cube[2]) in cubes:
        neighbours += 1

    surface += 6 - neighbours

print(surface)

# task 2

max_x += 1
max_y += 1
max_z += 1

min_x -= 1
min_y -= 1
min_z -= 1

surface = 0

outer_surface = set()

node = (max_x, max_y, max_z)

discovered, discovered_set, i = [node], {node}, 0
while i < len(discovered):
    node = discovered[i]
    cube_neighbours = 0
    node_neigbours = [  (node[0] + 1, node[1], node[2]), 
                        (node[0] - 1, node[1], node[2]), 
                        (node[0], node[1] + 1, node[2]), 
                        (node[0], node[1] - 1, node[2]), 
                        (node[0], node[1], node[2] + 1), 
                        (node[0], node[1], node[2] - 1)]
    for neighbour in node_neigbours:
        if neighbour[0] > max_x or neighbour[1] > max_y or neighbour[2] > max_z or neighbour[0] < min_x or neighbour[1] < min_y or neighbour[2] < min_z:
            continue
        if neighbour in cubes:
            outer_surface.add(node)
            continue
        if neighbour not in discovered_set:
            discovered.append(neighbour)
            discovered_set.add(neighbour)
    i += 1


# So much code duplication, but at least it works! 

for cube in outer_surface:
    neighbours = 0

    if (cube[0], cube[1], cube[2] - 1) in cubes:
        neighbours += 1
    
    if (cube[0], cube[1], cube[2] + 1) in cubes:
        neighbours += 1

    if (cube[0], cube[1] - 1, cube[2]) in cubes:
        neighbours += 1
    
    if (cube[0], cube[1] + 1, cube[2]) in cubes:
        neighbours += 1
    
    if (cube[0] - 1, cube[1], cube[2]) in cubes:
        neighbours += 1
    
    if (cube[0] + 1, cube[1], cube[2]) in cubes:
        neighbours += 1

    surface += neighbours

print(surface)
