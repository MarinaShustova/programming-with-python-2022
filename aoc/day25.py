lines = []
with open('day25.txt', 'r') as data:
    lines = data.readlines()

# Define the dictionaries
snafu_to_num = {"=":-2, "-":-1, "1":1, "0":0, "2":2}
num_to_snafu = {-2:"=", -1:"-", 1:"1", 2:"2", 0:"0"}

max_length = 0
snafu_inputs = []

for line in lines:
    curr_str = line.strip()
    snafu_inputs.append(curr_str)
    if len(curr_str) > max_length:
        max_length = len(curr_str)

def sum_result(col_totals):
    for i in range(len(col_totals)):
        if col_totals[i] > 2:
            try:
                col_totals[i + 1] += 1
                col_totals[i] -= 5
                return sum_result(col_totals)
            except:
                col_totals.append(1)
                col_totals[i] -= 5
                return sum_result(col_totals)
        if col_totals[i] < -2:
            try:
                col_totals[i + 1] -= 1
                col_totals[i] += 5
                return sum_result(col_totals)
            except:
                col_totals.append(-1)
                col_totals[i] += 5
                return sum_result(col_totals)

    return col_totals

col_totals = []
for i in range(1, max_length + 1):
    col_total = 0
    for input in snafu_inputs:
        if i <= len(input): 
            col_total += snafu_to_num[input[-i]]
    col_totals.append(col_total)

sum_result(col_totals)

col_totals.reverse()
final_string = ''

for i in col_totals:
    final_string += num_to_snafu[i]

print(final_string)
