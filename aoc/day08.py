tree_map = []
with open('day08.txt', 'r') as data:
    lines = data.readlines()
    for line in lines:
        new_line = line.strip()
        tree_map.append(list(map(int, new_line)))

n = len(tree_map)
m = len(tree_map[0])

# task 1

visible_count = n * 2 + m * 2 - 4

for i in range(1, n-1):
    for j in range(1, m-1):
        height = tree_map[i][j]
        i_th_row = tree_map[i]
        j_th_col = [col[j] for col in tree_map]
        if max(i_th_row[:j]) < height or max(j_th_col[:i]) < height or max(i_th_row[j+1:]) < height or max(j_th_col[i+1:]) < height:
            visible_count += 1

print(visible_count)

# task 2
max_sc_score = 0
for i in range(1, n-1):
    for j in range(1, m-1):
        height = tree_map[i][j]
        i_th_row = tree_map[i]
        j_th_col = [col[j] for col in tree_map]
        sc_score = 1
        
        cnt = 0
        k = j - 1
        while k >= 0 and i_th_row[k] < height:
            cnt += 1
            k -= 1
        cnt = cnt + 1 if k >= 0 else cnt
        sc_score *= cnt
        
        cnt = 0
        k = i - 1
        while k >= 0 and j_th_col[k] < height:
            cnt += 1
            k -= 1
        cnt = cnt + 1 if k >= 0 else cnt
        sc_score *= cnt

        cnt = 0
        k = j + 1
        while k < m and i_th_row[k] < height:
            cnt += 1
            k += 1
        cnt = cnt + 1 if k < m else cnt
        sc_score *= cnt

        cnt = 0
        k = i + 1
        while k < n and j_th_col[k] < height:
            cnt += 1
            k += 1
        cnt = cnt + 1 if k < n else cnt
        sc_score *= cnt

        if max_sc_score < sc_score:
            max_sc_score = sc_score

print(max_sc_score)
