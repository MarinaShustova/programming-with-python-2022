from itertools import cycle
from copy import deepcopy

line = ''
with open('day17.txt', 'r') as data:
    line = list(data.readlines()[0].strip())


def horizontal_line():
    fig = [['.', '@', '@', '@', '@', '.', '.']]
    space = [['.' for _ in range(7)] for _ in range(3)]
    fig.extend(space)
    fig.reverse()
    return fig

def plus():
    fig = [['.', '.', '.', '@', '.', '.', '.'],
            ['.', '.', '@', '@', '@', '.','.'],
            ['.', '.', '.', '@', '.', '.', '.']]
    space = [['.' for _ in range(7)] for _ in range(3)]
    fig.extend(space)
    fig.reverse()
    return fig

def angle():
    fig = [['.', '.', '@', '.', '.', '.', '.'],
            ['.', '.', '@', '.', '.', '.','.'],
            ['.', '.', '@', '@', '@', '.', '.']]
    space = [['.' for _ in range(7)] for _ in range(3)]
    fig.extend(space)
    fig.reverse()
    return fig

def vertical_line():
    fig = [['.', '.', '.', '.', '@', '.', '.'],
            ['.', '.', '.', '.', '@', '.','.'],
            ['.', '.', '.', '.', '@', '.', '.'],
            ['.', '.', '.', '.', '@', '.', '.']]
    space = [['.' for _ in range(7)] for _ in range(3)]
    fig.extend(space)
    fig.reverse()
    return fig

def cube():
    fig = [['.', '.', '.', '@', '@', '.', '.'],
            ['.', '.', '.', '@', '@', '.','.']]
    space = [['.' for _ in range(7)] for _ in range(3)]
    fig.extend(space)
    fig.reverse()
    return fig

chamber = [['#' for _ in range(7)]]

def push_a_rock(fig_height, bottom_index, direction):
    highest_point = len(chamber)
    match direction:
        case '<':
            for i in range(bottom_index, bottom_index+fig_height):
                for j in range(7):
                    if chamber[-(highest_point - i)][j] == '@' and (j == 6 or chamber[-(highest_point - i)][j+1] == '#'):
                        return chamber
            for i in range(bottom_index, bottom_index+fig_height):
                for j in range(5, -1, -1):
                    if chamber[-(highest_point - i)][j] == '@':
                        chamber[-(highest_point - i)][j+1] = chamber[-(highest_point - i)][j]
                        chamber[-(highest_point - i)][j] = '.'
        case '>':
            for i in range(bottom_index, bottom_index+fig_height):
                for j in range(7):
                    if chamber[-(highest_point - i)][j] == '@' and (j == 0 or chamber[-(highest_point - i)][j-1] == '#'):
                        return chamber
            for i in range(bottom_index, bottom_index+fig_height):
                for j in range(1, 7):
                    if chamber[-(highest_point - i)][j] == '@':
                        chamber[-(highest_point - i)][j-1] = chamber[-(highest_point - i)][j]
                        chamber[-(highest_point - i)][j] = '.'

# returns (True, bottom_index) and moves the rock down if possible, 
# returns (False, bottom_index) and marks the rock is landed otherwise
def move_down_if_possible(fig_height, bottom_index):
    highest_point = len(chamber)
    last_fig_row = chamber[bottom_index]
    next_row = chamber[bottom_index - 1]
    can_move = True
    for i in range(7):
        if last_fig_row[i] == '@' and next_row[i] == '#':
            can_move = False
    # manually check + in case its lower part doesn't meet an obsticle and the side one does
    if can_move == True and fig_height == 3:
        prev_last_row = chamber[bottom_index+1]
        prev_next_row = chamber[bottom_index]
        for i in range(7):
            if prev_last_row[i] == '@' and prev_next_row[i] == '#':
                can_move = False
    
    if not can_move:
        for i in range(bottom_index, bottom_index+fig_height+1):
            for j in range(7):
                if chamber[-(highest_point - i)][j] == '@':
                    chamber[-(highest_point - i)][j] = '#'
        return False, None

    for i in range(bottom_index, bottom_index + fig_height, 1):
            for j in range(7):
                if chamber[-(highest_point - i)][j] == '@':
                    chamber[-(highest_point - i) - 1][j] = '@'
                    chamber[-(highest_point - i)][j] = '.'

    is_last_row_empty = True
    for i in range(7):
        if chamber[-1][i] != '.':
            is_last_row_empty = False
            break
    if is_last_row_empty:
        del chamber[-1]
    bottom_index -= 1
    return True, bottom_index

hl = horizontal_line()
pl = plus()
ang = angle()
vl = vertical_line()
cb = cube()
figs_order = [hl, pl, ang, vl, cb]
figures = cycle(figs_order)
shifts = cycle(line)

rocks_to_fall = 2022

while rocks_to_fall > 0:
    current_fig = next(figures)

    chamber.extend(deepcopy(current_fig))

    in_the_air = True

    bottom_index = len(chamber) - (len(current_fig) - 3) 

    while in_the_air:
        shift = next(shifts)
        push_a_rock(len(current_fig)-3, bottom_index, shift)
        in_the_air, bottom_index = move_down_if_possible(len(current_fig)-3, bottom_index)
    
    rocks_to_fall -= 1
        

print(len(chamber) - 1)
