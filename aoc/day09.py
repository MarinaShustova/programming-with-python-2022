lines = []
with open('test.txt', 'r') as data:
    lines = data.readlines()

def parse_step(step):
    if step[0] == 'R':
        return int(step[1]), 0
    if step[0] == 'L':
        return -1 * int(step[1]), 0
    if step[0] == 'U':
        return 0, int(step[1])
    if step[0] == 'D':
        return 0, -1 * int(step[1])

def move_to_zero(pos):
    if pos > 0:
        return pos - 1
    return pos + 1

class Point:

    pos_x = 0
    pos_y = 0

    def __init__(self, x, y):
        self.pos_x = x
        self.pos_y = y

    def move(self, move_x, move_y):
        self.pos_x += move_x
        self.pos_y += move_y

    def is_touching(self, other):
        dist_x = abs(self.pos_x - other.pos_x)
        dist_y = abs(self.pos_y - other.pos_y)
        return dist_x <= 1 and dist_y <= 1

class Rope:

    def __init__(self, head, tail):
        self.head = head
        self.tail = tail

    def perform_step(self, step):
        positions = set()
        step_x, step_y = parse_step(step)
        while step_x != 0:
            pos = self.perform_single_step(step_x - move_to_zero(step_x), 0)
            step_x = move_to_zero(step_x)
            positions.add(pos)
        while step_y != 0:
            pos = self.perform_single_step(0, step_y - move_to_zero(step_y))
            step_y = move_to_zero(step_y)
            positions.add(pos)
        return positions


    def perform_single_step(self, step_x, step_y):
        self.head.move(step_x, step_y)
        if not self.head.is_touching(self.tail):
            dist_x = self.head.pos_x - self.tail.pos_x
            dist_y = self.head.pos_y - self.tail.pos_y
            # self row or column
            if dist_x == 0 or dist_y == 0:
                if dist_x == 0:
                    self.tail.move(0, -1 if dist_y < 0 else 1)
                elif dist_y == 0:
                    self.tail.move(-1 if dist_x < 0 else 1, 0)
            # diagonal
            else:
                if dist_x > 0 and dist_y > 0:
                    self.tail.move(1, 1)
                elif dist_x > 0 and dist_y < 0:
                    self.tail.move(1, -1)
                elif dist_x < 0 and dist_y < 0:
                    self.tail.move(-1, -1)
                elif dist_x < 0 and dist_y > 0:
                    self.tail.move(-1, 1)
        return self.tail.pos_x, self.tail.pos_y
            
# task 1

rope = Rope(Point(0,0), Point(0,0))
visited_positions = set()
visited_positions.add((0,0))
for step in lines:
    new_pos_set = rope.perform_step(step.strip().split(' '))
    visited_positions = visited_positions.union(new_pos_set)

print(len(visited_positions))

# task 2

class MultiKnotsRope(Rope):
    def __init__(self, ropes):
        self.ropes = ropes

    def perform_step(self, step):
        positions = set()
        step_x, step_y = parse_step(step)
        while step_x != 0:
            for i in range(len(self.ropes)):
                pos = self.ropes[i].perform_single_step(step_x - move_to_zero(step_x), 0)
                step_x = move_to_zero(step_x)
                positions.add(pos)
        while step_y != 0:
            pos = self.perform_single_step(0, step_y - move_to_zero(step_y))
            step_y = move_to_zero(step_y)
            positions.add(pos)
        return positions

    # def move_rope(self, i, move_x, move_y):


# H - 1, 1 - 2, 2 - 3, 3 - 4, 4 - 5, 5 - 6, 6 - 7, 7 - 8, 8 - 9
rope = MultiKnotsRope([Rope(Point(0,0), Point(0,0)), 
        Rope(Point(0,0), Point(0,0)), 
        Rope(Point(0,0), Point(0,0)), 
        Rope(Point(0,0), Point(0,0)), 
        Rope(Point(0,0), Point(0,0)), 
        Rope(Point(0,0), Point(0,0)), 
        Rope(Point(0,0), Point(0,0)), 
        Rope(Point(0,0), Point(0,0)), 
        Rope(Point(0,0), Point(0,0)) ])

visited_positions = set()
visited_positions.add((0,0))

for step in lines:
    new_pos_set = rope.perform_step(step.strip().split(' '))
    visited_positions = visited_positions.union(new_pos_set)