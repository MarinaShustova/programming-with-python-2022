lines = []
with open('day15.txt', 'r') as data:
    lines = data.readlines()

# For this task I got the general idea from 
# https://www.youtube.com/watch?v=w7m48_uCvWI&ab_channel=WilliamY.Feng
# because my own initial approach was quite inefficient 

y = 2000000
cnt = 0
beacons_on_the_line = 0
x_s = set()
beacons = set()

def convert_to_int(position):
    if not position[-1].isdigit():
        position = position[:-1]
    position = position[2:]
    return int(position)

for line in lines:
    words = line.strip().split(' ')
    sensor_pos = (convert_to_int(words[2]), convert_to_int(words[3]))
    beacon_pos = (convert_to_int(words[8]), convert_to_int(words[9]))

    md = abs(sensor_pos[0] - beacon_pos[0]) + abs(sensor_pos[1] - beacon_pos[1])            

    if md - abs(sensor_pos[1] - y) >= 0:
        dx = md - abs(sensor_pos[1] - y)
        left = sensor_pos[0] - dx
        right = sensor_pos[0] + dx
        interval = set(range(left, right + 1))
        x_s = x_s.union(interval)
        if beacon_pos[1] == y and not beacon_pos in beacons:
            beacons_on_the_line += 1
    
    beacons.add(beacon_pos)

cnt = len(x_s) - beacons_on_the_line

print(cnt)
