import copy

lines = []
with open('day14.txt', 'r') as data:
    lines = data.readlines()

start_of_the_abyss = -1

# As it was adviced in the hint
# Probably could be a list, since we are only using keys, but a dict is faster (?)
cave = {}

for line in lines:
    positions = line.strip().split(' -> ')
    start_pos = None
    pos_count = len(positions)
    for i in range(pos_count-1):
        current_pos = [int(x) for x in positions[i].split(',')]
        next_pos = [int(x) for x in positions[i + 1].split(',')]

        # Find the lowest point
        if next_pos[1] > start_of_the_abyss:
            start_of_the_abyss = next_pos[1]

        dx = current_pos[0] - next_pos[0]
        dy = current_pos[1] - next_pos[1]
        it_x = abs(dx) + 1
        it_y = abs(dy) + 1

        if dx != 0:
            while it_x > 0:
                key = (current_pos[0], current_pos[1])        
                if not (key in cave):
                    cave[key] = 1
                current_pos[0] = current_pos[0] + 1 if dx < 0 else current_pos[0] - 1
                it_x -= 1
        if dy != 0:
            while it_y > 0:
                key = (current_pos[0], current_pos[1])        
                if not (key in cave):
                    cave[key] = 1
                current_pos[1] = current_pos[1] + 1 if dy < 0 else current_pos[1] - 1
                it_y -= 1


cave_2 = copy.deepcopy(cave)

initial_pos = (500, 0)
resting_sand_units = 0

while True:
    abyss_reached = False
    curr = initial_pos
    while True:
        if not ((curr[0], curr[1] + 1) in cave):
            curr = (curr[0], curr[1] + 1)
        elif not ((curr[0] - 1, curr[1] + 1) in cave):
            curr = (curr[0] - 1, curr[1] + 1)
        elif not ((curr[0] + 1, curr[1] + 1) in cave):
            curr = (curr[0] + 1, curr[1] + 1)
        else:
            cave[curr] = 1
            resting_sand_units += 1
            break
        if curr[1] > start_of_the_abyss:
            abyss_reached = True
            break
    if abyss_reached:
        break
    
print(resting_sand_units)


# task 2
resting_sand_units = 0
floor = start_of_the_abyss + 2

cave = cave_2

while True:
    all_units_are_in_place = False
    curr = initial_pos
    while True:
        if not ((curr[0], curr[1] + 1) in cave) and curr[1] + 1 < floor:
            curr = (curr[0], curr[1] + 1)
        elif not ((curr[0] - 1, curr[1] + 1) in cave) and curr[1] + 1 < floor:
            curr = (curr[0] - 1, curr[1] + 1)
        elif not ((curr[0] + 1, curr[1] + 1) in cave) and curr[1] + 1 < floor:
            curr = (curr[0] + 1, curr[1] + 1)
        elif curr[0] == initial_pos[0] and curr[1] == initial_pos[1]:
            resting_sand_units += 1
            all_units_are_in_place = True
            break
        else:
            cave[curr] = 1
            resting_sand_units += 1
            break
    if all_units_are_in_place:
        break
    
print(resting_sand_units)

