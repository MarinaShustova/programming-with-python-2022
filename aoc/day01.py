# task 1
with open('day01.txt', 'r') as data:
    max_total = 0
    curr_total = 0
    all_lines = data.readlines()
    for line in all_lines:
        if line == "" or line =="\n":
            if curr_total > max_total:
                max_total = curr_total
            curr_total = 0
            continue
        curr_total += int(line)
    print(max_total)

# task 2

with open('day01.txt', 'r') as data:
    all_elves = []
    curr_total = 0
    all_lines = data.readlines()
    for line in all_lines:
        if line == "" or line =="\n":
            all_elves.append(curr_total)
            curr_total = 0
            continue
        curr_total += int(line)
    print(sum(sorted(all_elves, reverse=True)[:3]))