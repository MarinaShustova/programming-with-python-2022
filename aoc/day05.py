lines = []
with open('day05.txt', 'r') as data:
    lines = data.readlines()

pile_num = int(len(lines[0]) / 4)

# task 1
setup = []
for i in range(pile_num):
    setup.append([])
i = 0
while not lines[i].startswith(' 1 '):
    for j in range(pile_num):
        if lines[i][4 * j + 1] != ' ':
            setup[j].append(lines[i][4 * j + 1])
    i += 1

for pile in setup:
    pile = pile.reverse()

for line in lines:
    if not line.startswith('move'):
        continue
    command = line.strip().split(' ')
    n = int(command[1])
    fr = int(command[3]) - 1
    to = int(command[5]) - 1
    while n:
        box = setup[fr].pop() 
        setup[to].append(box)
        n -= 1

print(''.join(map(lambda l: l[-1], setup)))


# task 2
setup = []
for i in range(pile_num):
    setup.append([])
i = 0
while not lines[i].startswith(' 1 '):
    for j in range(pile_num):
        if lines[i][4 * j + 1] != ' ':
            setup[j].append(lines[i][4 * j + 1])
    i += 1

for pile in setup:
    pile = pile.reverse()

for line in lines:
    if not line.startswith('move'):
        continue
    command = line.strip().split(' ')
    n = int(command[1])
    fr = int(command[3]) - 1
    to = int(command[5]) - 1

    setup[to].extend(setup[fr][-n:])
    del setup[fr][-n:]


print(''.join(map(lambda l: l[-1], setup)))
