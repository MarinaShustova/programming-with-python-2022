import functools

lines = []
with open('day13.txt', 'r') as data:
    lines = data.readlines()


def compare_lists(left, right):
    if isinstance(left, int) and isinstance(right, int):
        if left < right:
            return 1
        elif left == right:
            return 0
        else:
            return -1
    if isinstance(left, list) and not isinstance(right, list):
        right = [right]
    elif isinstance(right, list) and not isinstance(left, list):
        left = [left]
    if len(left) == 0 and len(right) != 0:
        return 1
    elif len(right) == 0 and len(left) != 0:
        return -1
    elif len(left) == 0 and len(right) == 0:
        return 0
    len_l = len(left)
    len_r = len(right)
    for j in range(len_l):
        res = compare_lists(left[j], right[j])
        if res == 0:
            len_r -= 1
            if len_r == 0 and j < len_l - 1:
                return -1
            elif len_r == 0 and j == len_l - 1: 
                return 0
        else:
            return res
    return 1


n = len(lines)

right_order_pairs = 0

sum_ind = 0

for i in range(0, n, 3):
    lexp = eval(lines[i])
    rexp = eval(lines[i+1])
    res = compare_lists(lexp, rexp)
    if res == 1:
        sum_ind += i/3 + 1
        right_order_pairs += 1


print(sum_ind)

# task 2

str_list = list(filter(lambda x: x != '\n', lines))
str_list = [eval(x) for x in str_list]
str_list.append([[2]])
str_list.append([[6]])

srtd = sorted(str_list, key=functools.cmp_to_key(compare_lists), reverse=True)

print((srtd.index([[2]]) + 1) * (srtd.index([[6]]) + 1))