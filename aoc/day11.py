lines = []
with open('day11.txt', 'r') as data:
    lines = data.readlines()

n = len(lines)+1
# each monkey has 6 info lines + one division line
num_monkeys = n // 7 
monkeys = []
monkey_counters = [0 for _ in range(num_monkeys)]

def monk_mult(a, b):
    if b == 'old':
        b = a
    return a * int(b)

def monk_sum(a, b):
    return a + int(b)


for i in range(num_monkeys):
    monkey_properties = {}
    items = lines[i * 7 + 1].strip().split(':')[1].split(',')
    monkey_properties['starting_items'] = [int(x.strip()) for x in items]
    
    operation_expr = lines[i * 7 + 2].strip().split(':')[1].split('=')[1].split()
    if operation_expr[1] == '+':
        monkey_properties['operation'] = (monk_sum, operation_expr[2])
    elif operation_expr[1] == '*':
        monkey_properties['operation'] = (monk_mult, operation_expr[2])
    
    monkey_properties['test'] = int(lines[i * 7 + 3].strip().split()[-1])
    monkey_properties['true'] = int(lines[i * 7 + 4].strip().split()[-1])
    monkey_properties['false'] = int(lines[i * 7 + 5].strip().split()[-1])

    monkeys.append(monkey_properties)

rounds = 20

while rounds > 0:
    for i in range(num_monkeys):
        if not monkeys[i]['starting_items']:
            continue
        for item in monkeys[i]['starting_items']:
            monkey_counters[i] += 1
            new_level = monkeys[i]['operation'][0](item, monkeys[i]['operation'][1])
            new_level //= 3
            if new_level % monkeys[i]['test'] == 0:
                monkeys[monkeys[i]['true']]['starting_items'].append(new_level)
            else:
                monkeys[monkeys[i]['false']]['starting_items'].append(new_level)
        monkeys[i]['starting_items'] = []
    rounds -= 1

print(sorted(monkey_counters, reverse=True)[0] * sorted(monkey_counters, reverse=True)[1])


# task 2
# All divisions are prime numbers, we can work with that 

rounds = 10000
monkey_counters = [0 for _ in range(num_monkeys)]

monkeys = []

def get_remainders(tests, n):
    return [ n % test for test in tests]

def monk_mult_2(a, b, tests):
    if b == 'old':
        for i in range(len(a)):
            a[i] = (a[i] * a[i]) % tests[i]
    else:
        for i in range(len(a)):
            a[i] = (a[i] * int(b)) % tests[i]
    return a

def monk_sum_2(a, b, tests):
    for i in range(len(a)):
        a[i] = (a[i] + int(b)) % tests[i]
    return a

for i in range(num_monkeys):
    monkey_properties = {}
    items = lines[i * 7 + 1].strip().split(':')[1].split(',')
    monkey_properties['starting_items'] = [int(x.strip()) for x in items]
    
    operation_expr = lines[i * 7 + 2].strip().split(':')[1].split('=')[1].split()
    if operation_expr[1] == '+':
        monkey_properties['operation'] = (monk_sum_2, operation_expr[2])
    elif operation_expr[1] == '*':
        monkey_properties['operation'] = (monk_mult_2, operation_expr[2])
    
    monkey_properties['test'] = int(lines[i * 7 + 3].strip().split()[-1])
    monkey_properties['true'] = int(lines[i * 7 + 4].strip().split()[-1])
    monkey_properties['false'] = int(lines[i * 7 + 5].strip().split()[-1])

    monkeys.append(monkey_properties)


monkeys_tests = [m['test'] for m in monkeys]


for monkey in monkeys:
    monkey['starting_items'] = [get_remainders([test for test in monkeys_tests], x) for x in monkey['starting_items']]


while rounds > 0:
    for i in range(num_monkeys):
        if not monkeys[i]['starting_items']:
            continue
        for item in monkeys[i]['starting_items']:
            monkey_counters[i] += 1
            new_level = monkeys[i]['operation'][0](item, monkeys[i]['operation'][1], monkeys_tests)
            if new_level[i] == 0:
                monkeys[monkeys[i]['true']]['starting_items'].append(new_level)
            else:
                monkeys[monkeys[i]['false']]['starting_items'].append(new_level)
        monkeys[i]['starting_items'] = []
    rounds -= 1

print(sorted(monkey_counters, reverse=True))
print(sorted(monkey_counters, reverse=True)[0] * sorted(monkey_counters, reverse=True)[1])
