lines = []
with open('day21.txt', 'r') as data:
    lines = data.readlines()

monkey_tree = {}
operations = {}
nums = {}

def monk_sum(a, b):
    return a + b

def monk_mult(a, b):
    return a * b

def monk_div(a, b):
    return a / b

def monk_sub(a, b):
    return a - b


for line in lines:
    name_and_job = line.strip().split(':')
    name = name_and_job[0]
    operation = name_and_job[1].strip().split()

    if len(operation) == 1 and operation[0].isdigit():
        operations[name] = int(operation[0])
        monkey_tree[name] = []
        nums[name] = int(operation[0])
        continue

    left, op, right = operation[0].strip(), operation[1].strip(), operation[2].strip()
    monkey_tree[name] = [left, right]
    nums[name] = 0
    match op:
        case '+':
            operations[name] = monk_sum
        case '*':
            operations[name] = monk_mult
        case '-':
            operations[name] = monk_sub
        case '/':
            operations[name] = monk_div


def calculate_monkeys(monkey):
    if len(monkey_tree[monkey]) == 0:
        return operations[monkey]
    left = calculate_monkeys(monkey_tree[monkey][0])
    right = calculate_monkeys(monkey_tree[monkey][1])
    return operations[monkey](left, right)

root_num = calculate_monkeys('root')

print(root_num)
