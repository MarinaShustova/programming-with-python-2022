lines = []
with open('day03.txt', 'r') as data:
    lines = data.readlines()

# task 1
total_prior = 0
for line in lines:
    n = len(line.strip())
    half = int(n/2)
    comp_1 = set(line[:half])
    comp_2 = set(line[half:])
    overlap = next(iter(comp_1 & comp_2))
    total_prior += (27 + ord(overlap) - ord('A')) if overlap.isupper()  else (1 + ord(overlap) - ord('a'))
print(total_prior)

# task 2
total_prior = 0
n = len(lines)
for i in range(0, n-2, 3):
    overlap = next(iter(set(lines[i].strip()) & set(lines[i+1].strip()) & set(lines[i+2].strip())))
    total_prior += (27 + ord(overlap) - ord('A')) if overlap.isupper()  else (1 + ord(overlap) - ord('a'))
print(total_prior)