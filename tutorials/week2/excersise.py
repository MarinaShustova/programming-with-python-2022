#1
def my_max(ints):
    max_val = ints[0]
    for i in ints:
        if i > max_val:
            max_val = i
    return max_val

print([1, 5, 7, 9, 0, -1])
print(my_max([1, 5, 7, 9, 0, -1]))

#2
def process_list(ints):
    max_val = None
    for i in range(len(ints)):
        if ints[i] % 2 == 0:
            ints[i] /= 2
        else:
            ints[i] *= 2
        if i % 7 == 0:
            ints[i] += i
        if max_val is None or ints[i] > max_val:
            max_val = ints[i]
    return max_val

print([1, 3, 2])
print(process_list([1, 3, 2]))

#3
def collatz(my_int):
    values = []
    current_val = my_int
    values.append(current_val)
    while values.count(current_val) < 2:
        if current_val % 2 == 0:
            current_val /= 2
        else:
            current_val = current_val * 3 + 1
        # print(current_val)
        values.append(current_val)
    return values[-2]

print("COLLATZ")
print(collatz(7))        

#4
def compute_pi(steps_n):
    pi_curr = 0
    denom = -1
    for i in range(1, steps_n+1):
        #print(pi_curr)
        denom += 2
        pi_curr += (-1 if i % 2 == 0 else 1) / denom
    return pi_curr * 4

print("PI!!!!")
print(10000000)
print(compute_pi(10000000))

#5
def integer_cube_root(num):
    k = 0
    while k ** 3 < num:
       k += 1
    return k

print(3)
print(integer_cube_root(3))

#6
def caesar(data, shift):
    result = ""
    for c in data:
        if c == " ":
            result += c
            continue
        result += chr(ord(c) + shift)
    return result

print("HELLO WORLD")
print(caesar("HELLO WORLD", 3))

#7
def reverse_compliment(pattern):
    pairs = {"A":"T", "T":"A", "C":"G", "G":"C"}
    result = ""
    for char in pattern:
        result += pairs[char]
    return result[::-1]

print("ACGATCGATCGATTC")
print(reverse_compliment("ACGATCGATCGATTC"))

#8
def insertion_sort(list):
    i = 1
    n = len(list)
    while i < n:
        j = i
        while j > 0 and list[j-1] > list[j]:
            list[j-1], list[j] = list[j], list[j-1]
            j -= 1
        i += 1
    return list

print([3, 6, 7, 1])
print(insertion_sort([3, 6, 7, 1]))

#9
def fibonacci(n):
    if n == 0 or n == 1:
        return [1] if n == 0 else [1, 1]
    fib_i = 1
    fib_i_prev = 1
    fib_curr = 0
    fib_numbers = [fib_i, fib_i_prev]
    for i in range (2, n+1):
        fib_curr = fib_i_prev + fib_i
        fib_numbers.append(fib_curr)
        fib_i_prev, fib_i = fib_i, fib_curr
    return fib_numbers

print(4)
print(fibonacci(4))
