from collections import Counter

#1 
def count_duplicates(input):
    res = Counter(input.lower())
    return len([i for i in res if res[i] > 1])

print("Count duplicates")
print(count_duplicates("abBcdea"))

#2
def persistence(n):
    if n < 0:
        raise ValueError
    if n < 10:
        return 0
    counter = 0
    while len(str(n)) > 1:
        digits = str(n)
        n = 1
        for d in digits:
            n *= int(d)
        counter += 1
    return counter

print("Persistence")
print(persistence(999))

#2.2
def max_persistence():
    max_num = 0
    persistence_list = [persistence(n) for n in range(10000)]
    max_val = max(persistence_list)
    max_num = len(persistence_list) - persistence_list[::-1].index(max_val) - 1
    return max_num, max_val 

print("Max persistence")
print(max_persistence())

#Testing 1
def test_persistence():
    assert persistence(999) == 4

#3
dna_str = """
>ENA|AAF26411|AAF26411.1 TGEV spike protein expression construct truncated spike protein
ATGGCTTTCTTGAAGAGTTTCCCATTCTACGCTTTCTTGTGCTTCGGACAATACTTCGTG
GCTGTGACTCACGCAGACAACTTCCCATGCTCTAAGTTGACCAACAGGACCATCGGTAAT
CAATGGAACTTGATCGAGACCTTCTTGTTGAACTACTCATCTAGGTTGCCACCAAACTCT
GACGACGTGTTGGGTGACTACTTCCCAACTGTGCAACCTTGGTTCAACTGCATCAGGAAC
AACTCTAACGACTTGTACGTGACTTTGGAGAACTTGAAGGCTCTCTACTGGGACTACGCT
ACTGAGAACATCACCTGGAACCACAGGCAAAGGTTGAACGTGGTGGTGAACGGATACCCA
TACAGTATCACAGTGACAACAACCCGCAACTTCAACTCTGCTGAGGGTGCTATTATCTGC
ATTTGCAAGGGAAGTCCACCAACTACCACCACCGAGTCTAGTTTGACTTGCAACTGGGGA
AGTGAGTGCAGGTTGAACCACAAGTTCCCTATCTGTCCATCTAACTCAGAGGCAAACTGC
GGAAACATGCTGTACGGCTTGCAATGGTTCGCAGACGAGGTGGTGGCTTACTTGCATGGA
GCTAGTTACCGGATTAGTTTCGAGAACCAATGGTCTGGCACTGTGACATTCGGTGACATG
CGGGCCACAACATTGGAGGTGGCTGGCACGTTGGTGGACTTGTGGTGGTTCAACCCAGTG
TACGATGTCAGTTACTACAGGGTGAACAACAAGAACGGGACTACCGTGGTGAGCAACTGC
ACTGACCAATGCGCTAGTTACGTGGCTAACGTGTTCACTACACAGCCAGGAGGATTCATC
CCATCAGACTTTAGTTTCAACAACTGGTTCCTCTTGACTAACAGCAGCACTTTGGTGAGT
GGTAAGTTGGTGACCAAGCAGCCGTTGCTCGTTAACTGCTTGTGGCCAGTCCCAAGCTTC
GAGGAGGCAGCTTCTACATTCTGCTTCGAGGGAGCTGGCTTCGACCAATGCAATGGAGCT
GTGCTCAACAATACTGTGGACGTGATTAGGTTCAACCTCAACTTCACTACAAACGTGCAA
TCAGGGAAGGGTGCCACAGTGTTCTCATTGAACACAACCGGTGGAGTCACTCTCGAGATT
TCATGCTACACAGTGAGTGACTCGAGCTTCTTCAGTTACGGAGAGATTCCGTTCGGCGTG
ACTGACGGACCACGGTACTGCTACGTGCACTACAACGGCACAGCTCTCAAGTACCTCGGA
ACACTCCCACCTAGTGTGAAGGAGATTGCTATCAGTAAGTGGGGCCACTTCTACATTAAC
GGTTACAACTTCTTCAGCACATTCCCAATTGACTGCATCTCATTCAACTTGACCACTGGT
GACAGTGACGTGTTCTGGACAATCGCTTACACAAGCTACACTGAGGCACTCGTGCAAGTT
GAGAACACAGCTATTACAAAGGTGACGTACTGCAACAGTCACGTTAACAACATTAAGTGC
TCTCAAATTACTGCTAACTTGAACAACGGATTCTACCCTGTTTCTTCAAGTGAGGTTGGA
CTCGTGAACAAGAGTGTTGTGCTCCTCCCAAGCTTCTACACACACACCATTGTGAACATC
ACTATTGGGCTCGGAATGAAGCGTAGTGGGTACGGGCAACCAATCGCCTCAACATTGAGT
AACATCACATTGCCAATGCAGGACCACAACACCGATGTGTACTGCATTCGGTCTGACCAA
TTCTCAGTTTACGTGCATTCTACTTGCAAGAGTGCTTTGTGGGACAATATTTTCAAGCGA
AACTGCACGGACCACCACCATCACCATCACTAA
"""

def cleanup_data(ena_input):
    return ''.join(ena_input.split('\n', 2)[2].split('\n')).strip()

def occurences(data):
    return Counter(data)

def CG_content(occurences, n):
    return (occurences['G'] + occurences['C']) / n

def dna_str_info(input):
    data = cleanup_data(input)
    oc = occurences(data)
    return oc, CG_content(oc, len(data))

print("DNA info")
print(dna_str_info(dna_str))

#4
def list_kmers(s, k):
    if k > len(s):
        raise ValueError
    res = []
    for i in range(len(s)-k+1):
        res.append(s[i:i+k])
    return res

def number_of_unique_kmers(s, k):
    cntr = Counter(list_kmers(s, k))
    return len([kmer for kmer in cntr if cntr[kmer] == 1])

print("Kmers")
print(list_kmers('AAAA', 2))
print(number_of_unique_kmers('AAAA', 2))

#Testing 2
def test_list_kmers():
    assert list_kmers('ABABA', 3) == ['ABA', 'BAB', 'ABA']

def test_number_of_unique_kmers():
    assert number_of_unique_kmers('ABABA', 3) == 1
    assert number_of_unique_kmers('AAAA', 2) == 0

