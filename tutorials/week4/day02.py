part = 2

def row_value(row):
    for i in row:
        for j in row:
            # mod instead of is_integer 
            if i != j and (i/j).is_integer():
                return i//j

with open('input02.txt', 'r') as puzzle:
    lines_vals = []
    for line in puzzle:
        numbers = [int(n) for n in line.split('\t')]
        if part == 1:
            max_val = numbers[0]
            min_val = numbers[0]
            for num in numbers:
                if num > max_val:
                    max_val = num
                if num < min_val:
                    min_val = num
            lines_vals.append(max_val - min_val)
        elif part == 2:
            lines_vals.append(row_value(numbers))
    print(lines_vals)
    print(sum(lines_vals))

def test_1():
    assert row_value([5,9,2,8]) == 4
def test_2():
    assert row_value([9,4,7,3]) == 3 # and so on