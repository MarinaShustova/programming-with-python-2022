from collections import Counter


def readlines_fasta(file_name):
    content = ""
    with open(file_name, 'r') as f:
        content = "".join(line.strip() for line in f if not line.startswith('>'))
    return content 


def nucleotide_statistics(dna_string):
    return Counter(dna_string)


def parse_codons():
    codons = {}
    with open('04-genetic-code.txt', 'r') as codon_data:
        for line in codon_data:
            splitted_line = line.strip().split(' ')
            for codon in splitted_line[1:]:
                codons[codon] = splitted_line[0][:-1]
    # print(codons)
    return codons


def translate_dna_to_amino_acids(dna_string, codons_dict):
    n = len(dna_string)
    amino_acid_seqs = []
    on = False
    current_acid_seq = []
    i = dna_string.find('ATG')
    step = 1
    while i < n-3:
        current_codon = dna_string[i:i+3]
        if not on and codons_dict[current_codon] == 'M':
            on = True
            step = 3
        elif on and codons_dict[current_codon] == 'STOP':
            on = False
            amino_acid_seqs.append("".join(current_acid_seq))
            current_acid_seq = []
            step = 1
        if on:
            current_acid_seq.append(codons_dict[current_codon])
        i += step
    return amino_acid_seqs



def translate_first_valid_acid_from_dna(dna_string, codons_dict):
    start_index = dna_string.find('ATG')
    
    current_codon = dna_string[start_index : start_index+3]
    amino_string = [current_codon]
    i = start_index + 3
    while codons_dict[current_codon] != 'STOP':
        amino_string.append(dna_string[i:i+3])
        i += 3
        current_codon = dna_string[i:i+3]
    return "".join([codons_dict[codon] for codon in amino_string])


def print_list_to_file(data):
    with open('amino_acid_seqs.txt', 'w') as output:
        for seq in data:
            output.write("{}\n".format(seq))

if __name__ == "__main__":
    dna_string = readlines_fasta('sequence.fasta')
    codons = parse_codons()
    print(translate_first_valid_acid_from_dna(dna_string, codons))
    print_list_to_file(translate_dna_to_amino_acids(dna_string, codons))