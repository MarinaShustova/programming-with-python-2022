import math
#a
print(math.factorial(30) % 59)

#b
print((2**100) % 7)

#c
print(int("9"*99) // 25)

#d
print((33**33).bit_length())

#e
print(len(str(33**33)))

#f
print(max(abs(-10), abs(5), abs(20), abs(-35)))
print(min(abs(-10), abs(5), abs(20), abs(-35)))

#g
print(math.pi * (3**2))

#h
print(math.comb(20, 10))

#i
print(math.log2(3100000000))
