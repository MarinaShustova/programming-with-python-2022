from typing import List
import math

def modulo7(a):
    return a % 7

def sorted_mod_7(numbers: List):
    numbers.sort(key=modulo7, reverse=True)
    return numbers

def sorted_mod_7_lambda(numbers: List):
    numbers.sort(key=lambda x: x%7, reverse=True)
    return numbers

print(sorted_mod_7([5, 48, 98]))
print(sorted_mod_7_lambda([5, 48, 98]))

S =  (lambda x: sorted(x, key=lambda x: x%7, reverse=True))([5, 48, 98])

print(S)

def next_square(num):
    res = num
    while True:
        sqrt = math.sqrt(res)
        if sqrt % 1 == 0:
            return res
        res += 1

with open('numbers.txt', 'r') as numbers:
    line = numbers.readline().strip().split(' ')
    list_of_nums_map = list(map(int, line))
    # print("MAPPING")
    # print(list_of_nums_map[:10])
    list_of_nums_compr = [int(x) for x in line]
    # print("COMPREHENSION")
    # print(list_of_nums_compr[:10])
    print("Lists are the same: {}".format(list_of_nums_map == list_of_nums_compr))

    list_of_squares = list(map(lambda x: next_square(x), list_of_nums_map))
    print(sum(list_of_squares) == 4990535095484285)
